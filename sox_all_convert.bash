﻿#! /bin/bash

echo "Do you realy wish to set all files and folders to lower case?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) break;;
        No ) exit;;
    esac
done

for f in `find ./ -depth -name '*.wav'`
do

#    gsmdir=`dirname $f`/../gsm
#    `mkdir $gsmdir`
#    alawdir=`dirname $f`/../alaw
#    `mkdir $alawdir`

    gsmfile=${f//wav/gsm}
    alawfile=${f//wav/alaw}
    `sox $f -r 8k -c 1 -e gsm-full-rate $gsmfile`
    `sox $f -t raw -r 8k -c 1 -b 8 -e a-law $alawfile`
    `echo "$f -> $gsmfile" >> ./debug.txt`
done
