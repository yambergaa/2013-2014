﻿#! /bin/bash
#filter table
iptables --flush
iptables -A INPUT -i lo -j ACCEPT
iptables -A INPUT -i enp3s0 -j ACCEPT

#iptables -A INPUT -i ppp100 -j ACCEPT
#iptables -A INPUT -i ppp101 -j ACCEPT
#iptables -A INPUT -i ppp102 -j ACCEPT
#iptables -A INPUT -i ppp103 -j ACCEPT
iptables -A INPUT -s 192.168.5.0/24 -j ACCEPT
iptables -A INPUT -s 192.168.3.0/24 -j ACCEPT
iptables -A INPUT -s 192.168.0.0/24 -j ACCEPT
iptables -A INPUT -s 192.168.230.0/24 -j ACCEPT
iptables -A INPUT -s 192.168.221.0/24 -j ACCEPT

iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -P INPUT DROP

#nat table
iptables -t nat --flush
#iptables -t nat -A PREROUTING -p tcp -m tcp --dport 80 -j REDIRECT --to-ports 3128
#iptables -t nat -A POSTROUTING -s 192.170.0.0/24 -j MASQUERADE
#iptables -t nat -A POSTROUTING -s 192.168.230.0/24 -j MASQUERADE
