﻿#coding=utf-8
import logging
import datetime
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import time, csv
from dbfpy import dbf

#Инициализируем лог
logger = logging.getLogger('myapp')
hdlr = logging.FileHandler('myapp.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.INFO)

logger.info('Started') #Сообщаем что запустились
start = time.time() #Замер времени стартуем общий

profile = webdriver.FirefoxProfile()
profile.set_preference("permissions.default.images", 2) #Отрубаем картинки

browser = webdriver.Firefox(profile) # Get local session of firefox
browser.maximize_window()
browser.get("https://zab.cdmarf.ru") #Открываем нужную страницу и поехали

#Логинимся на сайте
browser.find_element_by_id("username").send_keys("YambergAA")
browser.find_element_by_id("password").send_keys("4adSDfwg2")
browser.find_element_by_id("submit").click()

i = 1 #Счетчик заполненных пациентов
r = 0 #Счетчик обработанных записей
l = 0 #Номер последней добавленной записи

def sex(w0):
	"Переварачиваем значение пола"
	if w0 == 1:
		w = "2"
	else:
		w = "1"
	return w

		
db = dbf.Dbf("amb.dbf") #Открываем наш хендл к базе с пациентами
for row in db:
	
	tmrow = time.time() #Обнуляем счетчик времени для каждой строки
	
	fam = row["FAM"]
	imya = row["IM"]
	ot = row["OT"]
	dr = row["DR"].strftime('%d.%m.%Y')
	w = sex(row["W"])

	r += 1
	if r <= l: 
		continue

	logger.info('~Total %d No %d Time from start %d  Time %d sec %s %s %s',r ,i, time.time()-start, tmrow, fam.decode('cp866'), imya.decode('cp866'), ot.decode('cp866'))
	
	#strname = unicode(fam,'utf8')+" "+unicode(imya,'utf8')+" "+unicode(ot,'utf8');
	#print i
	#print strname
	
	#i -= 1 #Уменьшаем счетчик на 1
			
	browser.get("https://zab.cdmarf.ru/pats/patients/new") #Заполнение карточки пациента
	browser.find_element_by_id("patient.individual.surname").send_keys(fam.decode('cp866')) #Фамилия
	browser.find_element_by_id("patient.individual.name").send_keys(imya.decode('cp866')) #Имя
	browser.find_element_by_id("patient.individual.patrName").send_keys(ot.decode('cp866')) #Отчество
	
	mdr = browser.find_element_by_xpath("//input[@id='patient.individual.birthDate']")
	mdr.click()
	mdr.send_keys(dr) #День варенья
		
	
	running = True
	while running:
		browser.find_element_by_link_text("search").click()
		time.sleep(1)
		try:
			browser.find_element_by_xpath("//li["+w+"]/a").click() #Выбираем наш пол
		except NoSuchElementException:
			continue #если элемент не найден идем в начало цикла
		else:
			running = False #Если все хорошо идем дальше
	
	#time.sleep(10)
	
	browser.find_element_by_xpath("//form/div[3]/div/div/div/span[2]/a").click() #Жмем далее //form/div/div/span[2]/a		
	time.sleep(2)
	
	try: #Если челове уже существует
		alert = browser.switch_to_alert()
		alert.dismiss()
		continue #Идем в начало цикла
	except:
		time.sleep(1)
	
	
	
	#ПРИКРЕПЛЕНИЕ
	browser.find_element_by_xpath("//fieldset[11]/table/tbody/tr/td[2]/div/div/div/div/span/a").click() #Кликаем ссылку прикрепления
	
	browser.find_element_by_xpath("//fieldset[11]/table/tbody/tr/td[2]/div/div[4]/table/tbody/tr/td[2]/div/div/div/div[2]/input").send_keys(unicode('гуз чит','utf8'))
	
	#Мо прекр
	running = True
	while running:
		
		browser.find_element_by_xpath("//fieldset[11]/table/tbody/tr/td[2]/div/div[4]/table/tbody/tr/td[2]/div/div/div/a").click()
		time.sleep(0.6)
		try:
			browser.find_element_by_xpath("//li/a").click()
		except NoSuchElementException:
			continue #если элемент не найден идем в начало цикла
		else:
			running = False #Если все хорошо идем дальше
	
	
	running = True
	while running:
		browser.find_element_by_xpath("//fieldset[11]/table/tbody/tr/td[2]/div/div[4]/table/tbody/tr[2]/td[2]/div/div/div/a").click()
		time.sleep(0.5)
		try:
			browser.find_element_by_link_text("Для постоянного динамического наблюдения").click() #вид п
		except NoSuchElementException:
			continue #если элемент не найден идем в начало цикла
		else:
			running = False #Если все хорошо идем дальше
	
	
	running = True
	while running:
		browser.find_element_by_xpath("//fieldset[11]/table/tbody/tr/td[2]/div/div[4]/table/tbody/tr[3]/td[2]/div/div/div/a").click()
		time.sleep(0.5)
		try:
			browser.find_element_by_link_text("Зарегистрирован").click()
		except NoSuchElementException:
			continue #если элемент не найден идем в начало цикла
		else:
			running = False #Если все хорошо идем дальше
			
	
	#Номер заявления о прик
	browser.find_element_by_xpath("//fieldset[11]/table/tbody/tr/td[2]/div/div[4]/table/tbody/tr[4]/td[2]/div/input").send_keys('-')
	
	now_date = datetime.date.today() # Текущая дата (без времени)
	date_str = now_date.strftime("%d.%m.%Y") #преобразовываем в нужный формат
	mod = browser.find_element_by_xpath("//input[@id='patient.patientRegs$requestDate']")
	mod.click()
	mod.send_keys(date_str)
	
	browser.find_element_by_xpath("//tr[13]/td[2]/div/div/span/a").click() #сохранить прек
	
	browser.find_element_by_xpath("//form/div[3]/div/div/div/span[2]/a").click() #Сохраняемся и выходим
	time.sleep(1)
	
	tmrow = time.time()-tmrow
	logger.info('Total %d No %d Time from start %d  Time %d sec %s %s %s',r ,i, time.time()-start, tmrow, fam.decode('cp866'), imya.decode('cp866'), ot.decode('cp866'))
	print 'Total %d No %d ,Time %d sec ,Time from start %d' % (r, i, tmrow, time.time()-start)
	i += 1 

db.close()

#logger.info(strname)
	
finish = time.time()
logger.info('Stoped')
print (finish-start)
#browser.close()
