﻿#! /bin/sh

PING=$[`ping -c 3 -s 1000 192.168.229.1 | grep received | awk -F, '{print $2}' |awk '{print $1}' `]

if [ $PING -eq 0 ]; then
    /etc/init.d/net.ppp100 restart
fi