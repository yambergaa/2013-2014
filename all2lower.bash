﻿#! /bin/bash

echo "Do you realy wish to set all files and folders to lower case?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) break;;
        No ) exit;;
    esac
done

for f in `find ./ -depth`
do
    tmp=`echo $f | tr '[A-Z]' '[a-z]'`
    tmp2=`dirname $f`/`basename $tmp`
    `echo "$f -> $tmp2" >> /home/debug.txt`
    `mv $f $tmp2`
done
